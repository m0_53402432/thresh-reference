package com.laolang.thresh.module.auth.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laolang.thresh.module.system.user.entity.SysUser;
import java.util.Collection;
import java.util.Set;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 登录用户.
 *
 * @author laolang
 * @version 0.1
 */
@NoArgsConstructor
@Data
public class LoginUser implements UserDetails {

    private Long id;
    private String username;
    private String tokenUuid;
    private String tokenValue;
    private String jwtSecret;
    private Set<String> perms;
    private Set<String> roles;
    private SysUser sysUser;

    public LoginUser(Set<String> perms, Set<String> roles, SysUser sysUser) {
        this.perms = perms;
        this.roles = roles;
        this.sysUser = sysUser;

        this.setId(sysUser.getId());
        this.username = sysUser.getUsername();
        this.jwtSecret = sysUser.getJwtSecret();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return sysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return username;
    }

    /**
     * 是否未过期.
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }


    /**
     * 是否未锁定.
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 是否过期的用户凭证.
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否可用.
     */
    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
