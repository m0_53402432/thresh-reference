package com.laolang.thresh;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@SpringBootApplication
public class ThreshBootJwtDemo {

    public static void main(String[] args) {
        SpringApplication.run(ThreshBootJwtDemo.class, args);
        log.info("thresh boot jwt demo is running...");
    }
}
