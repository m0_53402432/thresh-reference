package com.laolang.thresh.module.system.dict.service;

import com.laolang.thresh.framework.mybatis.core.BaseService;
import com.laolang.thresh.module.system.dict.entity.SysDictType;

/**
 * 系统字典 service.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysDictTypeService extends BaseService<SysDictType> {

}
