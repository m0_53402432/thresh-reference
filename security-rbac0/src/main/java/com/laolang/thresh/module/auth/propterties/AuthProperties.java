package com.laolang.thresh.module.auth.propterties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 鉴权配置.
 *
 * @author laolang
 * @version 0.1
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "thresh.auth")
public class AuthProperties {
    private String header;
    private String uuidKey;
    private String secret;
    private Integer accessExpire;
}