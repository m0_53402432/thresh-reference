package com.laolang.thresh.module.system.user.service;

import com.laolang.thresh.framework.mybatis.core.BaseService;
import com.laolang.thresh.module.system.user.entity.SysRole;

/**
 * 系统角色 service.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysRoleService extends BaseService<SysRole> {

}
