package com.laolang.thresh.module.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.laolang.thresh.framework.mybatis.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统菜单.
 *
 * @author laolang
 * @version 0.1
 */
@TableName("sys_menu")
@EqualsAndHashCode(callSuper = true)
@Data
public class SysMenu extends BaseEntity {

    private Long parentId;
    private Long appId;
    private String title;
    private Integer sort;
    private String path;
    private String component;
    private String query;
    private String type;
    private String frame;
    private String cache;
    private String visible;
    private String status;
    private String icon;
    private String perms;
    private Integer deleted;
}
