package com.laolang.thresh.module.system.user.service.impl;

import com.laolang.thresh.framework.mybatis.core.BaseServiceImpl;
import com.laolang.thresh.module.system.user.entity.SysMenu;
import com.laolang.thresh.module.system.user.mapper.SysMenuMapper;
import com.laolang.thresh.module.system.user.service.SysMenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统菜单 service 实现.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class SysMenuServiceImpl extends BaseServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    private final SysMenuMapper sysMenuMapper;
}
