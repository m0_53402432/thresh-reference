package com.laolang.thresh.framework.mybatis.consts;

import lombok.experimental.UtilityClass;

/**
 * mybatis 模块常量.
 *
 * @author laolang
 * @version 0.1
 */
@UtilityClass
public class MybatisConsts {

    public static final String LIMIT_1 = "limit 1";
}