package com.laolang.thresh.module.system.user.service.impl;

import com.laolang.thresh.framework.mybatis.core.BaseServiceImpl;
import com.laolang.thresh.module.system.user.entity.SysUser;
import com.laolang.thresh.module.system.user.mapper.SysUserMapper;
import com.laolang.thresh.module.system.user.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 系统用户 service 实现.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Service
public class SysUserServiceImpl
    extends BaseServiceImpl<SysUserMapper, SysUser>
    implements SysUserService {

    private final SysUserMapper sysUserMapper;
}
