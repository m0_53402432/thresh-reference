package com.laolang.thresh.module.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laolang.thresh.module.system.user.entity.SysUserLoginLog;

/**
 * 系统用户登录日志 dao.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysUserLoginLogMapper extends BaseMapper<SysUserLoginLog> {

}
