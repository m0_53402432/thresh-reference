package com.laolang.thresh.module.system.user.service;

import com.laolang.thresh.framework.mybatis.core.BaseService;
import com.laolang.thresh.module.system.user.entity.SysUserLoginLog;

/**
 * 系统用户登录日志 service.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysUserLoginLogService extends BaseService<SysUserLoginLog> {

}
