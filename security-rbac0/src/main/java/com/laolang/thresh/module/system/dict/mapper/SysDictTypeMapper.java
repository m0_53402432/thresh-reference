package com.laolang.thresh.module.system.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laolang.thresh.module.system.dict.entity.SysDictType;

/**
 * 系统字典 dao.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
