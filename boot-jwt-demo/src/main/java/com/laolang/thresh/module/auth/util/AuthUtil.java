package com.laolang.thresh.module.auth.util;

import com.laolang.thresh.module.auth.consts.AuthBizCode;
import com.laolang.thresh.module.auth.consts.AuthConsts;
import com.laolang.thresh.module.auth.dto.LoginUser;
import com.laolang.thresh.module.auth.exception.AuthBusinessException;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 鉴权业务工具类.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@Component
public class AuthUtil {

    public LoginUser getLoginUser(HttpServletRequest request) {
        Object loginUserObj = request.getAttribute(AuthConsts.LOGIN_USER_REQUEST_HEADER_KEY);
        if (loginUserObj instanceof LoginUser) {
            return (LoginUser) loginUserObj;
        }
        log.error(AuthBizCode.TOKEN_VERIFY_FAILED.getMsg());
        throw AuthBusinessException.authTokenVerifyFailed();
    }
}
