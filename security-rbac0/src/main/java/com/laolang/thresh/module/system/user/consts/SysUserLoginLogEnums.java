package com.laolang.thresh.module.system.user.consts;

import lombok.experimental.UtilityClass;

/**
 * 用户登录日志常量.
 *
 * @author laolang
 * @version 0.1
 */
@UtilityClass
public class SysUserLoginLogEnums {
    /**
     * 操作类型.
     *
     * @author laolang
     * @version 0.1
     */
    public enum OpType {
        IN("in", "登入"),
        OUT("out", "登出");
        private final String code;
        private final String desc;

        OpType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }

    /**
     * 操作来源.
     *
     * @author laolang
     * @version 0.1
     */
    public enum OpSource {
        USER("user", "用户"),
        SYSTEM("system", "系统");
        private final String code;
        private final String desc;

        OpSource(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDesc() {
            return desc;
        }
    }
}
