package com.laolang.thresh.module.system.dict.logic;

import cn.hutool.json.JSONUtil;
import com.laolang.thresh.module.auth.dto.LoginUser;
import com.laolang.thresh.module.auth.util.AuthUtil;
import com.laolang.thresh.module.system.dict.entity.SysDictType;
import com.laolang.thresh.module.system.dict.service.SysDictTypeService;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统字典管理逻辑实现.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class SysDictLogic {

    private final HttpServletRequest request;
    private final SysDictTypeService sysDictTypeService;
    private final AuthUtil authUtil;

    public SysDictType dictDetail() {
        log.info("system dict detail");
        LoginUser loginUser = authUtil.getLoginUser(request);
        log.info("loginUser:{}", JSONUtil.toJsonStr(loginUser));
        return sysDictTypeService.getById(1L);
    }
}
