package com.laolang.thresh.module.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.laolang.thresh.framework.mybatis.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统用户实体类.
 *
 * @author laolang
 * @version 0.1
 */
@TableName("sys_user")
@EqualsAndHashCode(callSuper = true)
@Data
public class SysUser extends BaseEntity {

    private Long tenantId;
    private String username;
    private String nickname;
    private String usertype;
    private String phone;
    private String email;
    private String gender;
    private String avatar;
    private String password;
    private String salt;
    private String jwtSecret;
    private String status;
    private Integer deleted;
}
