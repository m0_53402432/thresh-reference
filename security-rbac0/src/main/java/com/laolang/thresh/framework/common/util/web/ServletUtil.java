package com.laolang.thresh.framework.common.util.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/**
 * 客户端工具类.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@UtilityClass
public class ServletUtil {

    public static void renderJson(HttpServletResponse response, String json) throws IOException {
        //指定返回的格式为JSON格式
        response.setContentType("application/json;charset=utf-8");
        //setContentType与setCharacterEncoding的顺序不能调换，否则还是无法解决中文乱码的问题
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.write(json);
        out.close();
    }

    public static String getIpAddr(HttpServletRequest request) {

        String ip = request.getHeader("x-forwarded-for");

        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
            // log.error("X-Real-IP:" + ip);
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("http_client_ip");
            // log.error("http_client_ip:" + ip);
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            // log.error("getRemoteAddr:" + ip);
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
            // log.error("Proxy-Client-IP:" + ip);
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
            // log.error("WL-Proxy-Client-IP:" + ip);
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            // log.error("HTTP_X_FORWARDED_FOR:" + ip);
        }
        // 如果是多级代理，那么取第一个ip为客户ip
        if (ip != null && ip.contains(",")) {
            ip = ip.substring(ip.lastIndexOf(",") + 1).trim();
            // log.error("ip:" + ip);
        }
        return ip;
    }
}
