-- MySQL dump 10.13  Distrib 5.7.36, for Linux (x86_64)
--
-- Host: localhost    Database: thresh_reference
-- ------------------------------------------------------
-- Server version	5.7.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL COMMENT '字典类型编码',
  `group_code` varchar(100) DEFAULT NULL COMMENT '字典分组 (system: 系统字典)',
  `label` varchar(100) DEFAULT NULL COMMENT '字典标签',
  `value` varchar(100) DEFAULT NULL COMMENT '字典键值',
  `default_value` char(1) DEFAULT NULL COMMENT '是否默认值 (Y:是 N:否)',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统字典值表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES (1,'gender','system','男','0','Y','0','1','2023-12-19 02:51:34','1','2023-12-19 02:51:34',NULL,0,1),(2,'gender','system','女','1','N','0','1','2023-12-19 02:51:34','1','2023-12-19 02:51:34',NULL,0,1);
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `type` varchar(100) DEFAULT NULL COMMENT '字典类型编码',
  `group_code` varchar(100) DEFAULT NULL COMMENT '字典分组 (system: 系统字典)',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` VALUES (1,'用户性别','gender','system','0','1','2023-12-19 02:47:48','1','2023-12-19 02:47:48',NULL,0,1);
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单id',
  `app_id` bigint(20) DEFAULT NULL COMMENT '所属应用id',
  `title` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `sort` int(10) DEFAULT NULL COMMENT '排序值',
  `path` varchar(255) DEFAULT NULL COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) DEFAULT NULL COMMENT '路由参数',
  `type` char(1) DEFAULT NULL COMMENT '菜单类型 (M:目录 C:菜单 F:按钮)',
  `frame` char(1) DEFAULT NULL COMMENT '是否为外链 (Y:是 N:否)',
  `cache` char(1) DEFAULT NULL COMMENT '是否缓存 (Y:是 N:否)',
  `visible` char(1) DEFAULT NULL COMMENT '显示状态 (0:显示 1:隐藏)',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,0,1,'系统设置',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),(2,1,1,'菜单管理',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,'system:menu:list',NULL,NULL,NULL,NULL,NULL,0,NULL),(3,0,2,'项目管理',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,'eolinker:apiProject:list',NULL,NULL,NULL,NULL,NULL,0,NULL),(4,0,3,'商品管理',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,'mall:product:list',NULL,NULL,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'eolinkerAdmin','eolinker 管理员','0',NULL,NULL,NULL,NULL,NULL,0,1),(2,'mallAdmin','mall 管理员','0',NULL,NULL,NULL,NULL,NULL,0,1);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单id',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统角色菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (1,1,3,1),(2,2,4,1);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户 id',
  `username` varchar(100) DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(100) DEFAULT NULL COMMENT '用户昵称',
  `usertype` varchar(10) DEFAULT NULL COMMENT '用户类型',
  `phone` varchar(25) DEFAULT NULL COMMENT '手机号',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `gender` varchar(1) DEFAULT NULL COMMENT '性别',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `salt` varchar(255) DEFAULT NULL COMMENT '随机盐',
  `jwt_secret` varchar(150) DEFAULT NULL COMMENT 'jwt secret',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,1,'superAdmin','超级管理员','1','13500000001','superAdmin@thresh.com','x',NULL,'12345678',NULL,'pl3wbkeke066tgnqaxn4c6ezrju5wz9l','0','1','2024-01-19 11:41:32','1','2024-01-19 11:41:32','超级管理员',0,1),(2,2,'eolinkerAdmin','eolinker管理员','1','13500000002','eolinkerAdmin@thresh.com','x',NULL,'12345678',NULL,'dsswx48i9n9resl2f4ictvc5dapzj5dp','0','1','2024-01-19 11:41:32','1','2024-01-19 11:41:32','eolinker管理员',0,1),(3,3,'mallAdmin','mall管理员','1','13500000003','mallAdmin@thresh.com','x',NULL,'12345678',NULL,'52r7kd1gbd49rvkrt4hlju3bvgzrcvio','0','1','2024-01-19 11:41:32','1','2024-01-19 11:41:32','mallAdmin',0,1);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_login_log`
--

DROP TABLE IF EXISTS `sys_user_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `client_type` varchar(20) DEFAULT NULL COMMENT '客户端类型',
  `os` varchar(30) DEFAULT NULL COMMENT '操作系统',
  `browser` varchar(50) DEFAULT NULL COMMENT '浏览器',
  `op_datetime` datetime DEFAULT NULL COMMENT '登录时间',
  `op_type` varchar(10) DEFAULT NULL COMMENT '操作类型 (in:登入  , out:登出)',
  `op_source` varchar(10) DEFAULT NULL COMMENT '操作来源 (user:用户 ,system:系统)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='系统用户登录日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_login_log`
--

LOCK TABLES `sys_user_login_log` WRITE;
/*!40000 ALTER TABLE `sys_user_login_log` DISABLE KEYS */;
INSERT INTO `sys_user_login_log` VALUES (5,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:13:34','in','user',NULL,'2024-01-19 16:13:34',NULL,NULL,NULL,1),(6,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:18:59','in','user',NULL,'2024-01-19 16:18:59',NULL,NULL,NULL,1),(7,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:20:09','in','user',NULL,'2024-01-19 16:20:09',NULL,NULL,NULL,1),(8,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:21:19','in','user',NULL,'2024-01-19 16:21:19',NULL,NULL,NULL,1),(9,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:23:50','in','user',NULL,'2024-01-19 16:23:50',NULL,NULL,NULL,1),(10,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:25:08','in','user',NULL,'2024-01-19 16:25:08',NULL,NULL,NULL,1),(11,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:26:08','in','user',NULL,'2024-01-19 16:26:08',NULL,NULL,NULL,1),(12,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:27:17','in','user',NULL,'2024-01-19 16:27:17',NULL,NULL,NULL,1),(13,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:28:04','in','user',NULL,'2024-01-19 16:28:04',NULL,NULL,NULL,1),(14,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:30:56','in','user',NULL,'2024-01-19 16:30:56',NULL,NULL,NULL,1),(15,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:40:01','in','user',NULL,'2024-01-19 16:40:01',NULL,NULL,NULL,1),(16,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:40:36','in','user',NULL,'2024-01-19 16:40:36',NULL,NULL,NULL,1),(17,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:41:24','in','user',NULL,'2024-01-19 16:41:24',NULL,NULL,NULL,1),(18,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:42:01','in','user',NULL,'2024-01-19 16:42:01',NULL,NULL,NULL,1),(19,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:42:39','in','user',NULL,'2024-01-19 16:42:39',NULL,NULL,NULL,1),(20,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:42:58','in','user',NULL,'2024-01-19 16:42:58',NULL,NULL,NULL,1),(21,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:44:12','in','user',NULL,'2024-01-19 16:44:12',NULL,NULL,NULL,1),(22,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:44:15','out','user',NULL,'2024-01-19 16:44:15',NULL,NULL,NULL,1),(23,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:44:20','in','user',NULL,'2024-01-19 16:44:20',NULL,NULL,NULL,1),(24,1,'127.0.0.1','computer','Linux','firefox12','2024-01-19 16:57:26','in','user',NULL,'2024-01-19 16:57:26',NULL,NULL,NULL,1),(25,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 07:16:14','in','user',NULL,'2024-01-21 07:16:14',NULL,NULL,NULL,1),(26,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 07:16:58','in','user',NULL,'2024-01-21 07:16:58',NULL,NULL,NULL,1),(27,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:13:34','in','user',NULL,'2024-01-21 08:13:34',NULL,NULL,NULL,1),(28,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:13:43','in','user',NULL,'2024-01-21 08:13:43',NULL,NULL,NULL,1),(29,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:14:41','in','user',NULL,'2024-01-21 08:14:41',NULL,NULL,NULL,1),(30,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:15:44','in','user',NULL,'2024-01-21 08:15:44',NULL,NULL,NULL,1),(31,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:16:01','in','user',NULL,'2024-01-21 08:16:01',NULL,NULL,NULL,1),(32,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:16:32','in','user',NULL,'2024-01-21 08:16:32',NULL,NULL,NULL,1),(33,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:17:34','in','user',NULL,'2024-01-21 08:17:34',NULL,NULL,NULL,1),(34,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:17:42','in','user',NULL,'2024-01-21 08:17:42',NULL,NULL,NULL,1),(35,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:21:53','in','user',NULL,'2024-01-21 08:21:53',NULL,NULL,NULL,1),(36,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:22:17','in','user',NULL,'2024-01-21 08:22:17',NULL,NULL,NULL,1),(37,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:24:03','in','user',NULL,'2024-01-21 08:24:03',NULL,NULL,NULL,1),(38,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:26:35','in','user',NULL,'2024-01-21 08:26:35',NULL,NULL,NULL,1),(39,1,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:29:17','in','user',NULL,'2024-01-21 08:29:17',NULL,NULL,NULL,1),(40,3,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:33:17','in','user',NULL,'2024-01-21 08:33:17',NULL,NULL,NULL,1),(41,3,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:37:08','in','user',NULL,'2024-01-21 08:37:08',NULL,NULL,NULL,1),(42,2,'127.0.0.1','unknown','Unknown','unknown','2024-01-21 08:39:12','in','user',NULL,'2024-01-21 08:39:12',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `sys_user_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统用户角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,2,1,1),(2,3,2,1);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-21  0:41:37
