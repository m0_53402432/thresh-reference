package com.laolang.thresh.module.auth.controller;

import com.laolang.thresh.framework.common.core.domain.R;
import com.laolang.thresh.module.auth.logic.AuthLogic;
import com.laolang.thresh.module.auth.req.LoginReq;
import com.laolang.thresh.module.auth.rsp.LoginRsp;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 鉴权管理.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@RequestMapping("auth")
@RestController
public class AuthController {

    private final AuthLogic authLogic;

    @PostMapping("login")
    public R<LoginRsp> login(@Validated @RequestBody LoginReq req) {
        return R.ok(authLogic.login(req));
    }

    @GetMapping("logout")
    public R<Void> logout() {
        authLogic.logout();
        return R.ok();
    }
}
