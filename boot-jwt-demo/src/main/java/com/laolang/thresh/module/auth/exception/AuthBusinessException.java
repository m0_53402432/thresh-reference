package com.laolang.thresh.module.auth.exception;

import com.laolang.thresh.framework.common.core.consts.BizCode;
import com.laolang.thresh.framework.common.core.exception.BusinessException;
import com.laolang.thresh.module.auth.consts.AuthBizCode;

/**
 * 鉴权异常.
 *
 * @author laolang
 * @version 0.1
 */
public class AuthBusinessException extends BusinessException {

    public AuthBusinessException(BizCode bizCode) {
        super(bizCode);
    }

    public static AuthBusinessException authTokenVerifyFailed() {
        return new AuthBusinessException(AuthBizCode.TOKEN_VERIFY_FAILED);
    }

    public static AuthBusinessException loginFailedUsernamePasswordVerifyFailed() {
        return new AuthBusinessException(AuthBizCode.LOGIN_FAILED_USERNAME_PASSWORD_VERIFY_FAILED);
    }


}
