package com.laolang.thresh.framework.common.core.domain;

import com.laolang.thresh.framework.common.core.consts.DefaultStatusCode;
import com.laolang.thresh.framework.common.core.exception.BusinessException;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 统一返回结果.
 *
 * @author laolang
 * @version 0.1
 */
@Accessors(chain = true)
@Data
public class R<T> {

    /**
     * 接口请求结果的业务状态吗.
     */
    private String code;

    /**
     * 判断接口请求是否成功的唯一标识.
     */
    private Boolean success;

    /**
     * 提示信息.
     */
    private String msg;

    /**
     * 数据体.
     */
    private T body;

    /**
     * 扩充字段,正常情况下此字段为空，当此字段有值时，意味着当前接口结构不稳定，以后会修改,即保持 extra 为空.
     */
    private Object extra;

    public static <T> R<T> build(String code, boolean success, String msg, T body) {
        R<T> ajax = new R<>();
        ajax.setCode(code);
        ajax.setSuccess(success);
        ajax.setMsg(msg);
        ajax.setBody(body);
        ajax.setExtra(null);
        return ajax;
    }

    public void setPropFromBusinessException(BusinessException e) {
        setMsg(e.getMsg());
        setCode(e.getCode());
        setSuccess(false);
    }

    public static <T> R<T> ok() {
        return build(DefaultStatusCode.OK.getCode(), true, DefaultStatusCode.OK.getMsg(), null);
    }

    public static <T> R<T> ok(String code, String msg) {
        return build(code, true, msg, null);
    }

    public static <T> R<T> ok(String code, String msg, T body) {
        return build(code, true, msg, body);
    }

    public static <T> R<T> ok(T body) {
        return build(DefaultStatusCode.OK.getCode(), true, DefaultStatusCode.OK.getMsg(), body);
    }

    public static <T> R<T> failed() {
        return build(DefaultStatusCode.FAILED.getCode(), false, DefaultStatusCode.FAILED.getMsg(),
            null);
    }

    public static <T> R<T> failed(String msg) {
        return build(DefaultStatusCode.FAILED.getCode(), false, msg, null);
    }

    public static <T> R<T> doOverdue() {
        return build(DefaultStatusCode.OVERDUE.getCode(), false, DefaultStatusCode.OVERDUE.getMsg(),
            null);
    }

    public static <T> R<T> forbid() {
        return build(DefaultStatusCode.FORBID.getCode(), false, DefaultStatusCode.FORBID.getMsg(),
            null);
    }

    public static <T> R<T> error() {
        return build(DefaultStatusCode.ERROR.getCode(), false, DefaultStatusCode.ERROR.getMsg(),
            null);
    }

    public static <T> R<T> error(String msg) {
        return build(DefaultStatusCode.ERROR.getCode(), false, msg, null);
    }

    public static <T> R<T> error(String code, String msg) {
        return build(code, false, msg, null);
    }

    public static <T> R<T> doFixing() {
        return build(DefaultStatusCode.FIXING.getCode(), false, DefaultStatusCode.FIXING.getMsg(),
            null);
    }

    public static <T> R<T> notFound() {
        return build(DefaultStatusCode.NOT_FOUND.getCode(), false,
            DefaultStatusCode.NOT_FOUND.getMsg(),
            null);
    }

    public static <T> R<T> badRequest() {
        return build(DefaultStatusCode.BAD_REQUEST.getCode(), false,
            DefaultStatusCode.BAD_REQUEST.getMsg(), null);
    }

}