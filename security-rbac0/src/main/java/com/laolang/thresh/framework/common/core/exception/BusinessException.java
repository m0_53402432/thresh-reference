package com.laolang.thresh.framework.common.core.exception;

import com.laolang.thresh.framework.common.core.consts.BizCode;
import com.laolang.thresh.framework.common.core.consts.DefaultStatusCode;
import lombok.Getter;

/**
 * 业务异常基类,其他模块定义的异常需要继承此基类.
 *
 * @author laolang
 * @version 0.1
 */
@Getter
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final String code;
    private final String msg;

    public BusinessException(String message) {
        this.code = DefaultStatusCode.ERROR.getCode();
        this.msg = message;
    }

    public BusinessException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(String code, String msg, String message) {
        super(message);
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(BizCode bizCode) {
        super();
        this.code = bizCode.getCode();
        this.msg = bizCode.getMsg();
    }

}


