package com.laolang.thresh.module.auth.req;

import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 登录请求 dto.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class LoginReq {

    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
