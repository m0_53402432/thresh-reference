package com.laolang.thresh.module.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.laolang.thresh.framework.mybatis.core.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统用户登录日志实体类.
 *
 * @author laolang
 * @version 0.1
 */
@TableName("sys_user_login_log")
@EqualsAndHashCode(callSuper = true)
@Data
public class SysUserLoginLog extends BaseEntity {

    private Long userId;
    private String ip;
    private String clientType;
    private String os;
    private String browser;
    private LocalDateTime opDatetime;
    private String opType;
    private String opSource;
}
