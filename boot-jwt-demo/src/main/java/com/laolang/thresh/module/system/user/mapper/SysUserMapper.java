package com.laolang.thresh.module.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laolang.thresh.module.system.user.entity.SysUser;

/**
 * 系统用户 dao.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
