package com.laolang.thresh.framework.web;

import com.laolang.thresh.framework.common.core.domain.R;
import com.laolang.thresh.framework.common.core.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 全局异常拦截.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 业务异常.
     */
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<R<Void>> businessExceptionHandler(BusinessException e) {
        R<Void> r = R.failed();
        r.setPropFromBusinessException(e);
        log.error("请求出错:{}", r.getMsg());
        return ResponseEntity.status(HttpStatus.OK).body(r);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<R<Void>> accessDeniedExceptionHandler(AccessDeniedException e) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(R.forbid());
    }

    /**
     * 404.
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<R<Void>> notFoundHandler(NoHandlerFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(R.notFound());
    }

    /**
     * 405.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<R<Void>> methodNotAlloweddHandler(
        HttpRequestMethodNotSupportedException e) {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(R.badRequest());
    }


    /**
     * jsr 303.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<R<Void>> validatedBindException(MethodArgumentNotValidException e) {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(R.badRequest());
    }

    /**
     * 兜底异常处理.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<R<Void>> exceptionHandler(Exception e) {
        R<Void> r = R.error();
        log.error("未知异常");
        log.error(ExceptionUtils.getStackTrace(e));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(r);
    }
}
