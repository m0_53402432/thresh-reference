package com.laolang.thresh;

import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

/**
 * 临时测试.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class TmpTest {

    @Test
    public void testOne(){
        log.info(RandomUtil.randomString(32));
    }
}
