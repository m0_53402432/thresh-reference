package com.laolang.thresh.module.system.user.exception;

import com.laolang.thresh.framework.common.core.consts.BizCode;
import com.laolang.thresh.framework.common.core.exception.BusinessException;
import com.laolang.thresh.module.system.user.consts.SysUserBizCode;

/**
 * 系统用户业务异常.
 *
 * @author laolang
 * @version 0.1
 */
public class SysUserBusinessException extends BusinessException {

    public SysUserBusinessException(BizCode bizCode) {
        super(bizCode);
    }

    public static SysUserBusinessException userNotFoundByUsername() {
        return new SysUserBusinessException(SysUserBizCode.USER_NOT_FOUND_BY_USERNAME);
    }
}
