package com.laolang.thresh.module.auth.consts;

import com.laolang.thresh.framework.common.core.consts.BizCode;

/**
 * 鉴权业务状态码.
 *
 * @author laolang
 * @version 0.1
 */
public enum AuthBizCode implements BizCode {
    LOGIN_FAILED_USERNAME_PASSWORD_VERIFY_FAILED("login_failed_username_password_verify_failed", "用户名或密码错误"),
    TOKEN_VERIFY_FAILED("auth_token_verify_failed", "token 验证失败");

    /**
     * 业务状态码.
     */
    private final String code;

    /**
     * 提示信息.
     */
    private final String msg;

    AuthBizCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
