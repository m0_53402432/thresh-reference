package com.laolang.thresh.module.auth.dto;

import lombok.Data;

/**
 * 登录用户信息.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class LoginUser {

    private Long id;
    private String username;
    private String tokenUuid;
    private String tokenValue;
    private String jwtSecret;

}
