package com.laolang.thresh.module.auth.consts;

import lombok.experimental.UtilityClass;

/**
 * 鉴权业务通用常量.
 *
 * @author laolang
 * @version 0.1
 */
@UtilityClass
public class AuthConsts {


    /**
     * 超级管理员用户 id.
     */
    public static final Long SUPER_ADMIN_USER_ID = 1L;

    /**
     * 超级管理员用户名.
     */
    public static final String SUPER_ADMIN_USER_NAME = "superAdmin";

    /**
     * 超级管理员角色名.
     */
    public static final String SUPER_ADMIN_ROLE_NAME = "superAdmin";

    /**
     * 全部权限字符串.
     */
    public static final String ALL_PERMISSION = "*:*:*";

    /**
     * 权限字符串分隔符.
     */
    public static final String PERMISSION_DELIMETER = ",";

    /**
     * 登录用户在 request 中的属性名.
     */
    public static final String LOGIN_USER_REQUEST_HEADER_KEY = "loginUser";
}
