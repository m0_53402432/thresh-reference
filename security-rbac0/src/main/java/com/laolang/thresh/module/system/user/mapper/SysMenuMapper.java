package com.laolang.thresh.module.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laolang.thresh.module.system.user.entity.SysMenu;

/**
 * 系统菜单 dao.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
