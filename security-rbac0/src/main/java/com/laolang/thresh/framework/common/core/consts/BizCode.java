package com.laolang.thresh.framework.common.core.consts;

/**
 * 业务状态码接口,其他工程定义的业务状态码需实现此接口.
 *
 * @author laolang
 * @version 0.1
 */
public interface BizCode {

    /**
     * 业务异常状态码.
     */
    String getCode();

    /**
     * 业务异常描述.
     */
    String getMsg();
}
