package com.laolang.thresh.mall.product.controller;

import com.google.common.collect.Maps;
import com.laolang.thresh.framework.common.core.domain.R;
import java.util.Map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@RequestMapping("mall/product")
@RestController
public class ProductController {

    @PreAuthorize("@ss.hasPerm('mall:product:list')")
    @GetMapping("list")
    public R<Map<String, Object>> list() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("name", "mall product");
        return R.ok(map);
    }

}
