package com.laolang.thresh.module.system.user.consts;

import com.laolang.thresh.framework.common.core.consts.BizCode;

/**
 * 系统用户业务状态码.
 *
 * @author laolang
 * @version 0.1
 */
public enum SysUserBizCode implements BizCode {
    USER_NOT_FOUND_BY_USERNAME("user_not_found_by_username", "未通过用户名找到用户信息");
    /**
     * 业务状态码.
     */
    private final String code;

    /**
     * 提示信息.
     */
    private final String msg;

    SysUserBizCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
