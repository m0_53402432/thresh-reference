package com.laolang.thresh.module.auth.rsp;

import lombok.Data;

/**
 * 登录 vo.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class LoginRsp {

    String token;
    String uuid;
}
