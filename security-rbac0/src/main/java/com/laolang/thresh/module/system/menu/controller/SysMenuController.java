package com.laolang.thresh.module.system.menu.controller;

import com.google.common.collect.Maps;
import com.laolang.thresh.framework.common.core.domain.R;
import java.util.Map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统菜单接口.
 *
 * @author laolang
 * @version 0.1
 */
@RequestMapping("system/menu")
@RestController
public class SysMenuController {

    @PreAuthorize("@ss.hasPerm('system:menu:list')")
    @GetMapping("list")
    public R<Map<String, Object>> list() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("name", "system menu");
        return R.ok(map);
    }
}
