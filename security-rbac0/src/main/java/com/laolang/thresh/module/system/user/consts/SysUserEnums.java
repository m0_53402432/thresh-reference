package com.laolang.thresh.module.system.user.consts;

/**
 * 系统用户表常量.
 *
 * @author laolang
 * @version 0.1
 */
public class SysUserEnums {

    /**
     * 状态.
     *
     * @author laolang
     * @version 0.1
     */
    public enum Status {
        NORMAL("0", "正常"),
        LOCK("1", "锁定");
        private final String value;
        private final String desc;

        Status(String value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public String getValue() {
            return value;
        }

        public String getDesc() {
            return desc;
        }
    }

}
