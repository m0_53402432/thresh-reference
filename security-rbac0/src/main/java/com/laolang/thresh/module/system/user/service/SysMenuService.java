package com.laolang.thresh.module.system.user.service;

import com.laolang.thresh.framework.mybatis.core.BaseService;
import com.laolang.thresh.module.system.user.entity.SysMenu;

/**
 * 系统菜单 service.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysMenuService extends BaseService<SysMenu> {

}
