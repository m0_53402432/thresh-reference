package com.laolang.thresh.module.auth.handler;

import cn.hutool.json.JSONUtil;
import com.laolang.thresh.framework.common.core.domain.R;
import com.laolang.thresh.framework.common.util.web.ServletUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public class LoginFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
        AuthenticationException exception) throws IOException, ServletException {
        R<Void> r = R.failed(exception.getMessage());
        if (exception instanceof BadCredentialsException) {
            r.setMsg("用户名或密码错误");
        }
        ServletUtil.renderJson(response, JSONUtil.toJsonStr(r));
    }
}
