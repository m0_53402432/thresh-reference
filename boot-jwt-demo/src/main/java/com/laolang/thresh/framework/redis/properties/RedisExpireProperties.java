package com.laolang.thresh.framework.redis.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * redis 失效时长配置.
 *
 * @author laolang
 * @version 0.1
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "thresh.redis")
public class RedisExpireProperties {

    private Long authToken;
}
