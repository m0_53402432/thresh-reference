package com.laolang.thresh.eolinker.project.controller;

import com.google.common.collect.Maps;
import com.laolang.thresh.framework.common.core.domain.R;
import java.util.Map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@RequestMapping("eolinker/apiProject")
@RestController
public class ApiProjectController {

    @PreAuthorize("@ss.hasPerm('eolinker:apiProject:list')")
    @GetMapping("list")
    public R<Map<String, Object>> list() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("name", "eolinker project");
        return R.ok(map);
    }

}
