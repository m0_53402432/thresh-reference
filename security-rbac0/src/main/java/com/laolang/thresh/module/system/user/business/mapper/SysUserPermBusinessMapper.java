package com.laolang.thresh.module.system.user.business.mapper;

import java.util.Set;
import org.apache.ibatis.annotations.Param;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysUserPermBusinessMapper {

    Set<String> listRolesByUsername(@Param("username") String username);
}
