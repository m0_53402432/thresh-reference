package com.laolang.thresh.module.system.user.service;

import com.laolang.thresh.framework.mybatis.core.BaseService;
import com.laolang.thresh.module.system.user.entity.SysUser;

/**
 * 系统用户 service.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysUserService extends BaseService<SysUser> {

}
