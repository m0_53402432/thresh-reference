package com.laolang.thresh;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@SpringBootApplication
public class ThreshSecurityRbac0 {

    public static void main(String[] args) {
        SpringApplication.run(ThreshSecurityRbac0.class, args);
        log.info("thresh security rbac 0 is running...");
    }
}
