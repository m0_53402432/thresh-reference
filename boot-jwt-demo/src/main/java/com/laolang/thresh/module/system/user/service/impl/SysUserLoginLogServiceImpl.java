package com.laolang.thresh.module.system.user.service.impl;

import com.laolang.thresh.framework.mybatis.core.BaseServiceImpl;
import com.laolang.thresh.module.system.user.entity.SysUserLoginLog;
import com.laolang.thresh.module.system.user.mapper.SysUserLoginLogMapper;
import com.laolang.thresh.module.system.user.service.SysUserLoginLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 系统用户登录日志 service 实现.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Service
public class SysUserLoginLogServiceImpl
    extends BaseServiceImpl<SysUserLoginLogMapper, SysUserLoginLog>
    implements SysUserLoginLogService {

    private final SysUserLoginLogMapper sysUserLoginLogMapper;
}
