package com.laolang.thresh.module.system.user.business;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.laolang.thresh.framework.mybatis.consts.MybatisConsts;
import com.laolang.thresh.module.system.user.entity.SysUser;
import com.laolang.thresh.module.system.user.exception.SysUserBusinessException;
import com.laolang.thresh.module.system.user.service.SysUserService;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统用户常用业务方法.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class SysUserBusiness {

    private final SysUserService sysUserService;

    public SysUser assertExistByUsername(String username) {
        SysUser user = tryGetByUsername(username);
        if (Objects.isNull(user)) {
            log.error("未通过用户名:【{}】找到用户信息", username);
            throw SysUserBusinessException.userNotFoundByUsername();
        }
        return user;
    }

    public SysUser tryGetByUsername(String username) {
        return sysUserService.getOne(
            Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUsername, username).last(
                MybatisConsts.LIMIT_1));
    }

}
