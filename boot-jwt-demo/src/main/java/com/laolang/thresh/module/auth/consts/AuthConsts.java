package com.laolang.thresh.module.auth.consts;

/**
 * 鉴权业务通用常量.
 *
 * @author laolang
 * @version 0.1
 */
public class AuthConsts {

    public static final String LOGIN_USER_REQUEST_HEADER_KEY = "loginUser";
}
