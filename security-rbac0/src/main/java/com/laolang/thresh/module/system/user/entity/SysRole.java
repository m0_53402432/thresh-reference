package com.laolang.thresh.module.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.laolang.thresh.framework.mybatis.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统角色.
 *
 * @author laolang
 * @version 0.1
 */
@TableName("sys_role")
@EqualsAndHashCode(callSuper = true)
@Data
public class SysRole extends BaseEntity {

    private String code;
    private String name;
    private String status;
    private Integer deleted;
}
