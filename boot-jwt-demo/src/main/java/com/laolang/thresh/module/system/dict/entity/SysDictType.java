package com.laolang.thresh.module.system.dict.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.laolang.thresh.framework.mybatis.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统字典实体类.
 *
 * @author laolang
 * @version 0.1
 */
@TableName("sys_dict_type")
@EqualsAndHashCode(callSuper = true)
@Data
public class SysDictType extends BaseEntity {
    private String name;
    private String type;
    private String groupCode;
    private String status;
    private Integer deleted = 0;
}
