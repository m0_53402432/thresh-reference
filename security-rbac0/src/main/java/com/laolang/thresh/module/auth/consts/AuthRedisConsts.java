package com.laolang.thresh.module.auth.consts;

import lombok.experimental.UtilityClass;

/**
 * 鉴权业务 redis 缓存前缀.
 *
 * @author laolang
 * @version 0.1
 */
@UtilityClass
public class AuthRedisConsts {

    public static final String JWT_PREFIX = "auth_jwt_";
}
