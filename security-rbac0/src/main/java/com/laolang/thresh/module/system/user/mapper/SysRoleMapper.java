package com.laolang.thresh.module.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laolang.thresh.module.system.user.entity.SysRole;

/**
 * 系统角色 dao.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
