package com.laolang.thresh.module.system.dict.rsp;

import lombok.Data;


/**
 * 系统字典详情 vo.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysDictDetailRsp {

    private Long id;
    private String name;
    private String code;
}
