package com.laolang.thresh.module.auth.config;

import java.util.List;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * 缓存所有 {@link com.laolang.thresh.framework.web.annotations.AnonymousAccess} 注解标注的路径.
 *
 * @author laolang
 * @version 0.1
 */
@Component
@Data
public class AnonymousAccessBean {

    private List<String> uris;
}
