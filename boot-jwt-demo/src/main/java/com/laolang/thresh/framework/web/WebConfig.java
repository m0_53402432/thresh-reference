package com.laolang.thresh.framework.web;

import com.laolang.thresh.framework.web.interceptor.AuthInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web 配置.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Configuration
public class WebConfig implements WebMvcConfigurer {

    private final AuthInterceptor authInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor).addPathPatterns("/**");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            // 是否发送Cookie
            .allowCredentials(true)
            // 放行哪些原始域
            .allowedOrigins("*")
            // 放行哪些请求方式
            .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
            // 放行哪些原始请求头部信息
            .allowedHeaders("*")
            // 暴露哪些头部信息
            .exposedHeaders("*");
    }
}
